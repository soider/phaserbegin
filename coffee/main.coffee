player = null
walls = null
controls = null
playerSpeed = 400
layer = null


bullets = null
fireRate = 50
nextFire = 0

state = 
    preload:
        ->
            game.load.atlasJSONHash 'sprites', 'assets/sprites.png', 'assets/sprites.json'
            game.load.tilemap 'level1', 'assets/map1.json', null, Phaser.Tilemap.TILED_JSON
            game.load.image 'tiles', 'assets/tileset.jpg'

    create:
        ->
            game.stage.backgroundColor = '#ffffff';
            game.physics.startSystem Phaser.Physics.ARCADE
            map = game.add.tilemap 'level1'
            map.addTilesetImage 'tileset', 'tiles'
            map.setCollision 234
            layer = map.createLayer 'ground'
            layer.resizeWorld()
            player = game.add.sprite 100, 100, 'sprites', 'player01.png'
            game.physics.enable player
            player.body.collideWorldBounds = true
            player.body.bounce.setTo 1, 1
            player.anchor.setTo 0.5, 0.5
            player.animations.add 'blink', ['player01.png', 'player02.png']
            game.camera.follow(player);
            game.camera.deadzone = new Phaser.Rectangle 150, 150, 100, 100
            game.camera.focusOnXY 0, 0
            controls = 
                top: game.input.keyboard.addKey 87 # w
                bottom: game.input.keyboard.addKey 83 # s
                left: game.input.keyboard.addKey 65 # a 
                right: game.input.keyboard.addKey 68 # d

            bullets = game.add.group()
            bullets.enableBody = true
            bullets.physicsBodytype = Phaser.Physics.ARCADE
            bullets.createMultiple 300, 'sprites', 'bullet01.png', false

            bullets.setAll 'anchor.x', 0.5
            bullets.setAll 'anchor.y', 0.5
            bullets.setAll 'outOfBoundsKill', true
            bullets.setAll 'checkWorldBounds', true



    update:
        ->
            game.physics.arcade.collide player, layer
            player.animations.play 'blink'
            player.body.velocity.setTo 0, 0
            if controls.top.isDown
                player.body.velocity.y = -playerSpeed
            if controls.bottom.isDown
                player.body.velocity.y = playerSpeed
            if controls.left.isDown
                player.body.velocity.x = -playerSpeed
            if  controls.right.isDown
                player.body.velocity.x = playerSpeed
            if game.input.activePointer.isDown
                fire()

fire = ->
    if game.time.now > nextFire and bullets.countDead() > 0
        nextFire = game.time.now + fireRate
        bullet = bullets.getFirstExists false
        bullet.reset player.x, player.y
        bullet.rotation = game.physics.arcade.moveToPointer bullet, 1000, game.input.activePinter, 500


game = new Phaser.Game 500, 500, Phaser.AUTO, 'phaser-example', state


